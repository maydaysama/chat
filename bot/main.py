import hashlib
import time
from Auth import Auth
from Socket import Socket

print("Waiting for server...")

try:
	while True:
		try:
			auth = Auth("http://api:8000/user/login", {
				"mail": "bot@fnac.com",
				"password": hashlib.sha256("123456abc".encode()).hexdigest() })
		except Exception as e:
			print("Retry connection...")
			time.sleep(2)
		else:
			break
	sio = Socket("http://api:8080/", auth.token)
	sio.connect()
	sio.wait()
except Exception as e:
	print(e)
