from bs4 import BeautifulSoup
import requests
import re

def scrapGeniusCharts():
	headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", 
		"Accept-Encoding": "gzip, deflate, br", 
		"Accept-Language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,uz;q=0.6", 
		"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
	}
	page = requests.get("https://genius.com/", timeout=30, headers=headers)
	soup = BeautifulSoup(page.content, 'html.parser')
	articles = soup.find_all(class_="PageGriddesktop-a6v82w-0 ChartItemdesktop__Row-sc-3bmioe-0 izVEsw")
	releases = "Top Charts Genius:\n\n"
	for article in articles:
		titleCheck = article.find(class_="ChartSongdesktop__Title-sc-18658hh-3 WFLRu")
		titleName = "" if titleCheck == None else titleCheck.get_text().strip()
		artistCheck = article.find(class_="ChartSongdesktop__Artist-sc-18658hh-5 ffRKeh")
		artistName = re.sub("(\(Interprète\))", "", "" if artistCheck == None else artistCheck.get_text()).strip()
		releases += f'=======\nTitre: {titleName}\nArtiste: {artistName}\n'
	return releases