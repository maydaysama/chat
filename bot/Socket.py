import socketio
import requests
from scraping import scrapGeniusCharts
from parsing import parseMessage

class Socket():
    def __init__(self, url, authToken):
        self.sio = socketio.Client()
        self.url = url
        self.authToken = authToken
        @self.sio.event
        def connect():
            print("Bot connected")
        @self.sio.event
        def disconnect():
            print("Bot disconnected")
            self.sio.emit("disconnect")
        @self.sio.event
        def newMessage(data):
            message = data["content"]
            if parseMessage(message) == "genius":
                releases = scrapGeniusCharts()
                payload = { "content": releases }
                headers = { "Authorization": f'Bearer {self.authToken}' }
                send = requests.post("http://api:8000/message/", timeout=30, json=payload, headers=headers)
                print("Message sent")

    def connect(self):
        self.sio.connect(f'{self.url}?token={self.authToken}')
    def wait(self):
        self.sio.wait()