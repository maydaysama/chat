import jwt from 'jsonwebtoken';
import SHA256 from 'crypto-js/sha256';
import { futaba } from 'futaba';
import { userSchema } from "../schemes";
import { pgClient } from '../config';
import { parseError } from "../helper";
import { Schema } from "futaba/lib/types";
import { User } from "../types";

class UserEntity {
	scheme: Schema;
	constructor() {
		this.scheme = userSchema;
	};

	add(user: User): Promise<number> {
		return new Promise(async (res, rej) => {
			try {
				futaba(this.scheme, user);
				const { username, password, mail } = user;
				const response: any = await pgClient.query(`INSERT INTO "user" ("username", "password", "mail", "avatarId") VALUES ('${username}', '${SHA256(password || "").toString()}', '${mail}', '0') RETURNING "id";`);
				return res(response.rows[0]);
			}
			catch (e) {
				return rej(new Error(parseError(e.detail || e.message)));
			}
		});
	};

	getById(id: number): Promise<User> {
		return new Promise((res, rej) => {
			pgClient
			.query(`SELECT "id", "username", "mail", "avatarId" FROM "user" WHERE id = '${id}';`)
			.then(response => response.rowCount != 0 ? res(response.rows[0]) : rej(new Error("No users")))
			.catch(e => rej(new Error(parseError(e.detail || e.message))));
		});
	};

	login(user: User): Promise<string> {
		return new Promise(async (res, rej) => {
			try {
				const { mail, password } = user;
				const response: any = await pgClient.query(`SELECT id, username, mail, "avatarId" FROM "user" WHERE "mail" = '${mail}' AND "password" = '${password}';`);
				if (response.rowCount == 0)
					return rej(new Error("Wrong mail or password"));
				const { id, username, avatarId } = response.rows[0];
				const token: string = jwt.sign(
					{
						id,
						username,
						mail,
						avatarId,
						expiresAt: Date.now() + 1000000000 * 1000
					},
					"secretpass",
					{ algorithm: 'HS256'}
				);
				return res(token);
			}
			catch (e) {
				return rej(new Error("Cannot resolve query"));
			};
		});
	};

	modifyAvatar(userId: number, avatarId: number): Promise<number> {
		return new Promise((res, rej) => {
			pgClient
			.query(`UPDATE "user" SET "avatarId" = '${avatarId}' WHERE "id" = '${userId}';`)
			.then(response => res(avatarId))
			.catch(e => rej(new Error("Cannot modify avatar")));
		});
	};

	getAllUsers(): Promise<User[]> {
		return new Promise((res, rej) => {
			pgClient
			.query(`SELECT "username", "avatarId", "id", "mail" FROM "user";`)
			.then(response => res(response.rows))
			.catch(e => rej(new Error("Cannot get all users")));
		});
	};

	deleteUser(id: number): Promise<boolean> {
		return new Promise((res, rej) => {
			pgClient
			.query(`DELETE FROM "user" WHERE "id" = '${id}';`)
			.then(({ rowCount }) => rowCount ? res(true) : rej(new Error("No user deleted")))
			.catch(rej);
		});
	};
};

export default new UserEntity();