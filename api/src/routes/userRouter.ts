import express from 'express';
import { isAuth } from '../middleware';
import { UserEntity } from '../entities';
import { parseError } from '../helper';
import { checkToken } from '../middleware';

const userRouter = express.Router();

userRouter.post('/token', isAuth, (req, res) => {
	res.status(200).send("OK");
});

userRouter.post('/login', (req, res) => {
	UserEntity
	.login(req.body)
	.then(response => res.status(200).send(response))
	.catch(e => res.status(400).send(e.message));
});

userRouter.get('/', (req, res) => {
	UserEntity
	.getAllUsers()
	.then(response => res.status(200).send(response))
	.catch(e => res.status(400).send(e.message));
});

userRouter.get('/:id', (req, res) => {
	UserEntity
	.getById(parseInt(req.params.id))
	.then(response => res.status(200).send(response))
	.catch(e => res.status(400).send(e.message));
});

userRouter.post('/modifyAvatar', isAuth, async (req, res) => {
	if (req.headers.authorization)
	{
		const token = req.headers.authorization.split(" ")[1];
		try {
			const decodedToken: any = await checkToken(token);
			const response = await UserEntity.modifyAvatar(decodedToken.id, parseInt(req.body.avatarId));
			res.status(200).send({ avatarId: response });
		}
		catch (e) {
			res.status(400).send(e.message)
		};
	}
});

userRouter.post('/', (req, res) => {
	UserEntity
	.add(req.body)
	.then(response => res.status(200).send(response))
	.catch(e => res.status(400).send(e.message));
});

userRouter.delete('/:id', (req, res) => {
	UserEntity
	.deleteUser(parseInt(req.params.id))
	.then(response => res.status(200).send(response))
	.catch(e => res.status(400).send(e.message));
});

export default userRouter;