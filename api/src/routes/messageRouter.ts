import express from 'express';
import { Message } from '../models';
import { io } from '../config';
import { checkToken, isAuth } from '../middleware';

const messageRouter = express.Router();

messageRouter.get('/', (req, res) => {
	Message
	.find({}, null, { sort: { date: 1 } })
	.then(response => res.status(200).send(response))
	.catch(e => console.log(e));
});

messageRouter.post('/', isAuth, async (req, res) => {
	const { content } = req.body;
	if (req.headers.authorization)
	{
		const token: string = req.headers.authorization.split(" ")[1];
		try {
			const tokenDecoded: any = await checkToken(token);
			const message = new Message({
				content,
				userId: tokenDecoded.id,
			});
			const response = await message.save();
			res.status(200).json(response);
			io.sockets.emit("newMessage", response);
		}
		catch (e) {
			console.log(e);
			res.status(400).send(`Cannot add message: ${e.message}`)
		};
	}
});

export default messageRouter;