import express from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import cors from 'cors';
import messageRouter from '../routes/messageRouter';
import userRouter from '../routes/userRouter';

dotenv.config()

const app = express();

app.use(cors());
app.use(bodyParser.json())
app.use('/message', messageRouter);
app.use('/user', userRouter);

export default app;