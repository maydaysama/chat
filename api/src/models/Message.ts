import mongoose from 'mongoose'

const messageSchema = new mongoose.Schema({
	content: {
		type: String,
		required: true
	},
	userId: {
		type: Number,
		required: true,
	},
	date: {
		type: Date,
		default: () => new Date()
	},
});

export default mongoose.model("message", messageSchema);