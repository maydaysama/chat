import assert from 'assert';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import chaiHttp from 'chai-http';
import chai, { expect } from 'chai';
import { app, pgClient } from '../config';
import { Message } from '../models';
import { UserEntity } from '../entities';
import { checkToken } from '../middleware';
import SHA256 from 'crypto-js/sha256';
import { DecodedToken } from '../types';

const should = chai.should();
dotenv.config()

chai.use(chaiHttp);

let token: string | false = false;
let userId = 0;

interface UserTest {
	[key: string]: {
		[key: string]: any
	}
};

const user: UserTest = {
	valid: {
		username: "mehdi",
		mail: "dalil.mahdi@gmail.com",
		password: "24041993abc",
	},
	valid2: {
		username: "samee",
		mail: "samee.essentials@gmail.com",
		password: "24041993abc",
	},
	usernameAlreadyExisting: {
		username: "mehdi",
		mail: "another.mail@gmail.com",
		password: "24041993abc",
	},
	mailAlreadyExisting: {
		username: "anotheruser",
		mail: "dalil.mahdi@gmail.com",
		password: "24041993abc",
	},
	invalidUsername: {
		username: ")))#$   edeadea",
		mail: "dalil.mahdi@gmail.com",
		password: "24041993abc",
	},
	invalidMail: {
		username: "mehdi",
		mail: "@gmail.com",
		password: "24041993abc",
	},
	invalidPassword: {
		username: "mehdi",
		mail: "dalil.mahdi@gmail.com",
		password: "24041993",
	},
	notExisting: {
		mail: "yoshikage.kira@morio.jp",
		password: "kiraqueen2332",
	},
	falsePassword: {
		mail: "dalil.mahdi@gmail.com",
		password: "24041993def",
	},
};

before(done => {
	pgClient.connect()
		.then(() => {
			console.log("PostgreSQL: connnection OK !");
			pgClient.query(`DELETE FROM "user" WHERE "mail" != 'bot@fnac.com';`);
			mongoose.connect("mongodb://admin:admin@mongo-dev:27017", { useNewUrlParser: true, useUnifiedTopology: true })
			.then(() => {
				console.log('MongoDB: connnection OK !');
				Message.deleteMany({}, err => {});
				done();
			})
			.catch((e) => {
				console.log(`Unable to connect MongoDB: ${e}`);
			});
		})
		.catch((e) => {
			console.log(`Unable to connect PostgreSQL: ${e}`);
		});
});

describe("User", function () {
	describe("UserEntity", function () {
		describe("create", function () {
			it("should create a user", async function () {
				const response: any = await UserEntity.add(user.valid);
				const id = parseInt(response.id);
				id.should.be.an("number");
				userId = id;
			});
			it("bad username should throw an error", async function () {
				await assert.rejects(UserEntity.add(user.invalidUsername), {
					name: "Error",
					message: "username not valid !"
				});
			});
			it("bad mail should throw an error", async function () {
				await assert.rejects(UserEntity.add(user.invalidMail), {
					name: "Error",
					message: "mail not valid !"
				});
			});
			it("bad password should throw an error", async function () {
				await assert.rejects(UserEntity.add(user.invalidPassword), {
					name: "Error",
					message: "password not valid !"
				});
			});
			it("username already existing should throw an error", async function () {
				await assert.rejects(UserEntity.add(user.usernameAlreadyExisting), {
					name: "Error",
					message:"username \'mehdi\' already exists !"
				});
			});
			it("mail already existing should throw an error", async function () {
				await assert.rejects(UserEntity.add(user.mailAlreadyExisting), {
					name: "Error",
					message: "mail \'dalil.mahdi@gmail.com\' already exists !"
				});
			});
		});
		describe("login", function () {
			it("should reject non existing user", async function () {
				await assert.rejects(UserEntity.login(user.notExisting), {
					name: "Error",
					message: "Wrong mail or password"
				});
			});
			it("should reject bad password", async function () {
				await assert.rejects(UserEntity.login(user.falsePassword), {
					name: "Error",
					message: "Wrong mail or password"
				});
			});
			it("bad token should be rejected", async function () {
				await assert.rejects(checkToken("eldleadlal"), {
					name: "Error",
					message: "Invalid token !",
				});
			});
			it("should login existing user", async function () {
				const password = SHA256(user.valid.password).toString();
				token = await UserEntity.login({ ...user.valid, ["password"]: password});
				const dec: DecodedToken = await checkToken(token);
				dec.should.have.property("username");
				dec.username.should.be.equal("mehdi");
			});
		});
		describe("delete", function () {
			it("should delete user", async function () {
				const response = await UserEntity.deleteUser(userId);
				response.should.be.equal(true);
			});
		})
	});
	describe("UserAPI", function () {
		describe("/POST /user", function () {
			it("should create a user", function (done) {
				chai.request(app)
				.post("/user")
				.send(user.valid)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("id");
					res.body.id.should.be.a('number');
					user.valid.id = res.body.id;
					done();
				});
			});
			it("should not create a user with the same credentials", function (done) {
				chai.request(app)
				.post("/user")
				.send(user.valid)
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
			});
			it("should create a second user", function (done) {
				chai.request(app)
				.post("/user")
				.send(user.valid2)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("id");
					res.body.id.should.be.a('number');
					user.valid2.id = res.body.id;
					done();
				});
			});
		});
		describe("/DELETE /user", function () {
			it("should delete the second user", function (done) {
				chai.request(app)
				.delete(`/user/${user.valid2.id}`)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.equal(true);
					done();
				});
			});
			it("should return 400 if user does not exist", function (done) {
				chai.request(app)
				.delete(`/user/${user.valid2.id}`)
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
			});
		});
		describe("/GET /user/:id", function () {
			it("should return user by id", function (done) {
				chai.request(app)
				.get(`/user/${user.valid.id}`)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("id");
					res.body.id.should.be.a('number');
					done();
				});
			});
			it("bad id should return 400", function (done) {
				chai.request(app)
				.get(`/user/${user.valid2.id}`)
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
			});
		});
		describe("/GET /user", function () {
			it("should return all users", function (done) {
				chai.request(app)
				.get(`/user`)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.should.be.lengthOf(2);
					done();
				});
			});
		});
		describe("/POST /user/login", function () {
			it("should login user", function (done) {
				const password = SHA256(user.valid.password).toString();
				chai.request(app)
				.post(`/user/login`)
				.send({ ...user.valid, ["password"]: password })
				.end((err, res) => {
					res.should.have.status(200);
					token = res.text;
					done();
				});
			});
		});
		describe("/POST /user/modifyAvatar", function () {
			it("/POST should change user avatar", function (done) {
				chai.request(app)
				.post(`/user/modifyAvatar`)
				.set('Authorization', `Bearer ${token}`)
				.send({ avatarId: 5 })
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("avatarId");
					res.body.avatarId.should.be.equal(5);
					done();
				});
			});
		});
	});
});

describe("Message", function () {
	describe("MessageModel", function () {
		describe("create", function () {
			it("should return the message content when created", async function () {
				const body = { content: "MessageModel - test 1", userId: 1 };
				const message: any = new Message(body);
				const { content, userId } = await message.save();
				expect({ content, userId }).to.be.eql(body);
			});
			it("should return a ValidationError", async function () {
				const body = { content: "TEST" };
				const message = new Message(body);
				await assert.rejects(message.save(), { name: "ValidationError" });
			});
		});
		describe("get", function () {
			it("should return an array length 1", async function () {
				const messages = await Message.find({});
				expect(messages).to.have.lengthOf(1);
			});
		});
	});
	describe("MessageAPI", function () {
		describe("/GET /message", function () {
			it("it should GET all the messages", function (done) {
				chai.request(app)
				.get("/message")
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.should.be.lengthOf(1);
					done();
				});
			});
		});
		describe("/POST /message", function () {
			it("it should create a message with the token id", function (done) {
				chai.request(app)
				.post("/message")
				.set('Authorization', `Bearer ${token}`)
				.send({ content: "MessageAPI - test 2", userId: 56 })
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("content");
					res.body.content.should.be.equal("MessageAPI - test 2");
					done();
				});
			});
			it("should not create a message if token id is bad", function (done) {
				chai.request(app)
				.post("/message")
				.set('Authorization', `Bearer ekfeafkeadoaedkeaEDEAdae`)
				.send({ content: "MessageAPI - test 2", userId: 56 })
				.end((err, res) => {
					res.should.have.status(405);
					done();
				});
			});
		});
	});
});

after(done => {
	mongoose.disconnect();
	pgClient.end();
	done();
});