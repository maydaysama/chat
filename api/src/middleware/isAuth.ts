import checkToken from "./checkToken";
import { Response, Request } from "express";

const reject = (res: Response) => {
	res.status(405).send("You need to login to access this page !");
}

const isAuth = (req: Request, res: Response, next: any) => {
	if (req.headers.authorization) {
		const token = req.headers.authorization.split(" ")[1];

		checkToken(token)
		.then(tok => next())
		.catch(err => reject(res));
	}
	else {
		return reject(res);
	}
}

export default isAuth;