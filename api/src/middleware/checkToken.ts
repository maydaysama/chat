import jwt from 'jsonwebtoken';
import { DecodedToken } from '../types';

const checkToken = (token: string): Promise<DecodedToken> => new Promise((res, rej) => {
	jwt.verify(token, "secretpass", (err, dec: any) => {
		if (err)
			return rej(new Error("Invalid token !"));
		if (dec.expiresAt < Date.now())
			return rej(new Error("Expired !"));
		return res(dec);
	});
});

export default checkToken;