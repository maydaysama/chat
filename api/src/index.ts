import mongoose from 'mongoose';
import { pgClient, app, server } from './config';

server.listen(8080, () => {
	console.log('socket listening on 8080');
});

app.listen(8000, () => {
	console.log('node listening on 8000');
});

pgClient.connect()
	.then(() => console.log("PostgreSQL: connnection OK !"))
	.catch((e: any) => console.log(`Unable to connect PostgreSQL: ${e}`));

mongoose.connect("mongodb://admin:admin@mongo-dev:27017", { useNewUrlParser: true, useUnifiedTopology: true })
	.then(() => console.log('MongoDB: connnection OK !'))
	.catch((e: any) => console.log(`Unable to connect MongoDB: ${e}`));