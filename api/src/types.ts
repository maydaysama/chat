/**
 * user
 */

export type UserId = number;
export type UserMail = string;
export type UserPassword = string;
export type UserUsername = string;
export type UserAvatarId = number;

export interface User {
    id?: UserId,
    mail?: UserMail,
    password?: UserPassword,
    username?: UserUsername,
    avatarId?: UserAvatarId,
};

/**
 * token
 */
export interface DecodedToken {
    id: UserId,
    mail: UserMail,
    username: UserUsername,
    avatarId: UserAvatarId,
    expiresAt: number
};