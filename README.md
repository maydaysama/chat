# Chat
A little chat with a single room where the user can create a profile and change his avatar in a pic set already given.
### Deploy project for development
`./run`

### Project organization
This project has 3 subdivisions:
##### Front
The frontend is written in React.js.

##### Api
The backend and API is written in Node.js and is using Express of the HTTP Server.

It also contains a second HTTP Server for the SocketIO server-side client.

##### Bot
The bot is written in Python3 and is used to scrap informations that can be
called in the chat at any moment.

### Environment
Each service has a Docker container, initialized like this:
*  Front
*  Api
*  Bot
*  MongoDB (used to save the chat messages)
*  MongoExpress (used to admin MongoDB easily)
*  PostgreSQL (used to store users information)

