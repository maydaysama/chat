import { ADD_MESSAGE, addMessage, fetchMessages, FETCH_MESSAGES, thunkFetchMessages, messageReducer } from "../store";
import { Message } from "../types";
import { MessageApi } from "../api";

describe("Message reducer", () => {
    describe("ADD_MESSAGE", () => {
        test("should add message to reducer", () => {
            const data: Message = {
                _id: "343fd",
                content: "message",
                userId: 2,
                date: "24-04-1993"
            };
            expect(messageReducer([], { type: ADD_MESSAGE, payload: data })).toEqual([data]);
        });
    });
    describe("FETCH_MESSAGES", () => {
        test("should fetch messages to reducer", () => {
            const data: Message[] = [
                {
                    _id: "1kfeka",
                    content: "message",
                    userId: 1,
                    date: "24-04-1993"
                }
            ];
            expect(messageReducer([], { type: FETCH_MESSAGES, payload: data })).toEqual([...data]);
        });
    });
});