import { fetchUsers, thunkFetchUsers, FETCH_USERS } from "../store";
import { User } from "../types";
import { UserApi } from "../api";

describe("User actions", () => {
    describe("FETCH_USERS", () => {
        test("should fetch messages", () => {
            const data: User[] = [
                {
                    id: 4,
                    username: "mayday",
                    avatarId: 5,
                    mail: "dalil.mahdi@gmail.com"
                }
            ];
            const expectedAction = {
                type: FETCH_USERS,
                payload: data
            };
            expect(fetchUsers(data)).toEqual(expectedAction);
        });
    });
    describe("thunkFetchUsers", () => {
        test("should async fetch messages from db", (done) => {
            const data: User[] = [
                {
                    id: 4,
                    username: "mayday",
                    avatarId: 5,
                    mail: "dalil.mahdi@gmail.com"
                }
            ];
            UserApi.get = jest.fn().mockResolvedValue({ data });
            const response = thunkFetchUsers();
            const dispatch = jest.fn();
            response(dispatch);
            
            setImmediate(() => {
                expect(dispatch).toBeCalledWith({ type: FETCH_USERS, payload: data});
                done();
            });
        });
    });
});