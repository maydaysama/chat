import { fetchUsers, thunkFetchUsers, FETCH_USERS, usersReducer } from "../store";
import { User } from "../types";
import { UserApi } from "../api";

describe("User actions", () => {
    describe("FETCH_USERS", () => {
        test("should fetch messages", () => {
            const data: User[] = [
                {
                    id: 4,
                    username: "mayday",
                    avatarId: 5,
                    mail: "dalil.mahdi@gmail.com"
                }
            ];
            expect(usersReducer([], { type: FETCH_USERS, payload: data })).toEqual([...data]);
        });
    });
});