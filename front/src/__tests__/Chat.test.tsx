import React from 'react';
import { render, fireEvent, prettyDOM } from '@testing-library/react';
import { Send } from "../components/chat/Send";
import { MessageApi } from '../api';
import { Message } from '../types';
import { MessageList } from '../components/chat/MessageList';

jest.mock('../components/chat/Message', () => (props: any) => {
    return (
        <div>
            <div data-testid="id">{props.message._id}</div>
            <div data-testid="userId">{props.message.userId}</div>
            <div data-testid="content">{props.message.content}</div>
        </div>
    );
});

describe("<Chat />", () => {
    describe("<MessageList />", () => {
        test("should display message list", () => {
            window.HTMLElement.prototype.scrollIntoView = jest.fn();
            const messages: Message[] = [
                {
                    _id: "id1",
                    content: "message",
                    userId: 1,
                    date: "24-04-1993",
                }
            ];
            const mock = jest.fn();
            const component = render(<MessageList messages={messages} thunkFetchMessages={mock} thunkFetchUsers={mock} />);
            const id = component.getByTestId("id").textContent;
            const content = component.getByTestId("content").textContent;
            const userId = parseInt(component.getByTestId("userId").textContent || "");

            expect(id).toEqual(messages[0]._id);
            expect(content).toEqual(messages[0].content);
            expect(userId).toEqual(messages[0].userId);
        });
    });
    describe("<Send />", () => {
        test("should send message", () => {
            const mockAuth = jest.fn();
            MessageApi.post = jest.fn().mockResolvedValue({});

            const component = render(<Send session={{ token: "edadae" }} thunkCheckAuth={mockAuth} />);
            const field = component.getByTestId("field");
            const button = component.getByTestId("button");

            fireEvent.change(field, { target: { value: "    message     " } });
            fireEvent.click(button);

            expect(MessageApi.post).toBeCalledWith("/", {"content": "message"}, {"headers": {"Authorization": "Bearer edadae"}});
        });
        test("should send nothing", () => {
            const mockAuth = jest.fn();
            MessageApi.post = jest.fn().mockResolvedValue({});

            const component = render(<Send session={{ token: "edadae" }} thunkCheckAuth={mockAuth} />);
            const field = component.getByTestId("field");
            const button = component.getByTestId("button");

            fireEvent.change(field, { target: { value: "      " } });
            fireEvent.click(button);

            expect(MessageApi.post).toBeCalledTimes(0);
        });
        test("should check auth", () => {
            const mockAuth = jest.fn();
            MessageApi.post = jest.fn().mockRejectedValue("error");

            const component = render(<Send session={{ token: "aedadae" }} thunkCheckAuth={mockAuth} />);
            const field = component.getByTestId("field");
            const button = component.getByTestId("button");

            fireEvent.change(field, { target: { value: "test" } });
            fireEvent.click(button);

            expect(mockAuth).toBeCalledTimes(0);
        });
    });
});