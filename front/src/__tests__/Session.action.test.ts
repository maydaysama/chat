import { LOG_USER, logUser, DELOG_USER, delogUser, thunkCheckAuth } from "../store";
import { UserApi } from "../api";

describe("Session actions", () => {
    describe("LOG_USER", () => {
        test("should log user", () => {
            const data: string = "4aBkcdi4983lkffikzpeldjfue94840";
            const expectedAction = {
                type: LOG_USER,
                payload: data
            };
            expect(logUser(data)).toEqual(expectedAction);
        });
    });
    describe("DELOG_USER", () => {
        test("should delog user", () => {
            const expectedAction = {
                type: DELOG_USER
            };
            expect(delogUser()).toEqual(expectedAction);
        });
    });
    describe("thunkCheckAuth", () => {
        test("should check authentification and validate auth", (done) => {
            UserApi.post = jest.fn().mockResolvedValue({ });
            const response = thunkCheckAuth();
            const dispatch = jest.fn();
            const getState: any = () => ({ session: { token: "abcdefgh" } });
            response(dispatch, getState);
            
            setImmediate(() => {
                expect(UserApi.post).toBeCalledWith("/token", {}, {
					headers: {
						'Authorization': `Bearer ${getState().session.token}`,
					},
                });
                done();
            });
        });
        test("should check authentification and rejects it", (done) => {
            UserApi.post = jest.fn().mockRejectedValue({ });
            const response = thunkCheckAuth();
            const dispatch = jest.fn();
            const getState: any = () => ({ session: { token: "abcdefgh" } });
            response(dispatch, getState);
            
            setImmediate(() => {
                expect(dispatch).toBeCalledWith({ type: DELOG_USER });
                done();
            });
        });
    });
});