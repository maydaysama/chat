import React from 'react';
import { render, fireEvent, prettyDOM } from "@testing-library/react";
import { UserApi } from '../api';
import { Login } from '../components/account/Login';
import { Create } from '../components';

describe("<Login />", () => {
    test("should login and call action with request response ", (done) => {
        const mockLogUser = jest.fn();
        UserApi.post = jest.fn().mockResolvedValue({ data: "response" });

        const component = render(<Login logUser={mockLogUser} />);

        const input: any = component.container.querySelector("#mail");
        const password: any = component.container.querySelector("#password");
        const button: any = component.container.getElementsByClassName("MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedPrimary");
        
        fireEvent.change(input, { target: { value: 23 } });
        fireEvent.change(password, { target: { value: 24041993 } });
        fireEvent.click(component.getByText("OK"));
        
        setImmediate(() => {
            expect(UserApi.post).toBeCalledWith("/login", {"mail": "23", "password": "71b7c0aee702f56d4bfb39c31f13402700dd8b69072edfd7909ef492afe2aed6"});
            expect(mockLogUser).toBeCalledWith("response");
            done();
        });
    });
});

describe("<Create />", () => {
    test("should throw an error ", (done) => {
        const component = render(<Create />);
        const password: any = component.container.querySelector("#password");
        const passwordConfirm: any = component.container.querySelector("#passwordConfirm");
        const button: any = component.container.getElementsByClassName("MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedPrimary");

        fireEvent.change(password, { target: { value: "2404" } });
        fireEvent.change(passwordConfirm, { target: { value: "24041993" } });
        fireEvent.click(component.getByText("OK"));

        setImmediate(() => {
            const error: any = component.getByTestId("error").textContent;
            expect(error).toStrictEqual("You did'nt enter the same password !");
            done();
        });
    });

    test("should create user", (done) => {
        UserApi.post = jest.fn().mockResolvedValue({ data: "response" });

        const component = render(<Create />);
        const username: any = component.container.querySelector("#username");
        const mail: any = component.container.querySelector("#mail");
        const password: any = component.container.querySelector("#password");
        const passwordConfirm: any = component.container.querySelector("#passwordConfirm");
        const button: any = component.container.getElementsByClassName("MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedPrimary");

        fireEvent.change(username, { target: { value: "maydaysama" } });
        fireEvent.change(mail, { target: { value: "dalil.mahdi@gmail.com" } });
        fireEvent.change(password, { target: { value: "24041993abc" } });
        fireEvent.change(passwordConfirm, { target: { value: "24041993abc" } });
        fireEvent.click(component.getByText("OK"));

        setImmediate(() => {
            expect(UserApi.post).toBeCalledWith("/", {"mail": "dalil.mahdi@gmail.com", "username": "maydaysama", "password": "24041993abc"});
            done();
        });
    });
});