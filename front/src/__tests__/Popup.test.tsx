import React from 'react';
import { Popup } from "../components";
import { render, prettyDOM } from "@testing-library/react";
import { PopupType } from '../types';
import { renderIntoDocument } from 'react-dom/test-utils';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

const popups: PopupType = {
	test1: (setPopup) => ({
		message: "User successfully created ! Please login now !",
		buttons: [
			{ label: "OK", path: "/login" },
		],
    }),
    test2: (setPopup) => ({
		message: "What you wanna do ?",
		buttons: [
            { label: "Login", path: "/login" },
            { label: "Create", path: "/create" },
		],
	})
};

ReactDOM.createPortal = node => node as React.ReactPortal;

describe("<Popup />", () => {
    test("should display popup with message and button ok", () => {
        const component = render(<BrowserRouter><Popup info={popups.test1()} /></BrowserRouter>);
        const message: any = component.getByTestId("message").textContent;
        const button: any = component.getByTestId("button0").textContent;
        expect(message).toEqual("User successfully created ! Please login now !");
        expect(button).toEqual("OK");
    });
    test("should display popup with message and two buttons", () => {
        const component = render(<BrowserRouter><Popup info={popups.test2()} /></BrowserRouter>);
        const message: any = component.getByTestId("message").textContent;
        const button0: any = component.getByTestId("button0").textContent;
        const button1: any = component.getByTestId("button1").textContent;
        expect(message).toEqual("What you wanna do ?");
        expect(button0).toEqual("Login");
        expect(button1).toEqual("Create");
    });
});