import { LOG_USER, DELOG_USER, sessionReducer } from "../store";
import { UserApi } from "../api";
import { User } from "../types";

describe("Session actions", () => {
    describe("LOG_USER", () => {
        test("should log user", () => {
            const data: string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTA4LCJ1c2VybmFtZSI6Im1laGRpIiwibWFpbCI6ImRhbGlsLm1haGRpQGdtYWlsLmNvbSIsImF2YXRhcklkIjowLCJleHBpcmVzQXQiOjE1ODMzNTIwNTM4OTYsImlhdCI6MTU4MzM0MjA1M30.YtKUxft0r5t_X2GNunwb3QtPU6JDKbYiOz4jc68UVfY";
            const user: User[] = [
                {
                    id: 108,
                    username: "mehdi",
                    avatarId: 0,
                    mail: "dalil.mahdi@gmail.com"
                }
            ];
            expect(sessionReducer(
                {
                    token: false,
                    user: false
                },
                {
                    type: LOG_USER,
                    payload: data
                }))
                .toEqual(
                    {
                        token: data,
                        user: {
                            id: 108,
                            username: "mehdi",
                            avatarId: 0,
                            mail: "dalil.mahdi@gmail.com",
                            iat: 1583342053,
                            expiresAt: 1583352053896
                        }
                });
        });
    });
    describe("DELOG_USER", () => {
        test("should delog user", () => {
            expect(
                sessionReducer({
                    token: "ledeakdeaif",
                    user: {
                        id: 108,
                        username: "mehdi",
                        avatarId: 0,
                        mail: "dalil.mahdi@gmail.com"
                    }
                }, 
                {
                    type: DELOG_USER
                }))
                .toEqual({ token: false, user: false });
            });
    });
});