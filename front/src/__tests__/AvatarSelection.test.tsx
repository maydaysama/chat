import React from 'react';
import { render, prettyDOM, fireEvent } from '@testing-library/react';
import { AvatarSelection } from '../components/profile/AvatarSelection';
import { avatarReducer } from '../store';
import { UserApi } from '../api';

describe("<AvatarSelection />", () => {
    test("should list avatars", () => {
        const mock = jest.fn();
        const avatars = avatarReducer();
        const component = render(<AvatarSelection session={ { token: "aaa" }} avatars={avatars} delogUser={mock} />);

        const imgCheck = component.container.querySelector("#avatar7")?.getAttribute("src");
        expect(imgCheck).toEqual("undefined/resources/avatar/7.jpg");
    });
    test("should send avatarId", () => {
        const mock = jest.fn();
        const avatars = avatarReducer();
        UserApi.post = jest.fn().mockResolvedValue({});
        const component = render(<AvatarSelection session={ { token: "aaa" }} avatars={avatars} delogUser={mock} />);
        const img = component.getByTestId("avatar7")
        const button = component.getByTestId("button");
        fireEvent.click(img);
        fireEvent.click(button);
        expect(UserApi.post).toBeCalledWith("/modifyAvatar", {"avatarId": "0"}, {"headers": {"Authorization": "Bearer aaa"}});
    });
});