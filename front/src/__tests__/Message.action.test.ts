import { ADD_MESSAGE, addMessage, fetchMessages, FETCH_MESSAGES, thunkFetchMessages } from "../store";
import { Message } from "../types";
import { MessageApi } from "../api";

describe("Message actions", () => {
    describe("ADD_MESSAGE", () => {
        test("should add message", () => {
            const data: Message = {
                _id: "1kfeka",
                content: "message",
                userId: 1,
                date: "24-04-1993"
            };
            const expectedAction = {
                type: ADD_MESSAGE,
                payload: data
            };
            expect(addMessage(data)).toEqual(expectedAction);
        });
    });
    describe("FETCH_MESSAGES", () => {
        test("should fetch messages", () => {
            const data: Message[] = [
                {
                    _id: "1kfeka",
                    content: "message",
                    userId: 1,
                    date: "24-04-1993"
                }
            ];
            const expectedAction = {
                type: FETCH_MESSAGES,
                payload: data
            };
            expect(fetchMessages(data)).toEqual(expectedAction);
        });
    });
    describe("thunkFetchMessages", () => {
        test("should async fetch messages from db", (done) => {
            const data: Message[] = [
                {
                    _id: "1kfeka",
                    content: "message",
                    userId: 1,
                    date: "24-04-1993"
                }
            ];
            MessageApi.get = jest.fn().mockResolvedValue({ data });
            const response = thunkFetchMessages();
            const dispatch = jest.fn();
            response(dispatch);
            
            setImmediate(() => {
                expect(dispatch).toBeCalledWith({ type: FETCH_MESSAGES, payload: data});
                done();
            });
        });
    });
});