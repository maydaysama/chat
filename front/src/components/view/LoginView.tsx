import React from 'react';
import View from './View';
import { Login } from '../account';

const LoginView = () => {
	return (
		<View Component={Login} unlogged redirect="/" />
	);
};

export default LoginView;