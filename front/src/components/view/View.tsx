import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { thunkCheckAuth } from '../../store';

const View = ({ thunkCheckAuth, logged, unlogged, session, Component, redirect}: any) => {
	thunkCheckAuth();
	if (logged)
		return session.token ? <Component /> : <Redirect to={redirect} />;
	else if (unlogged)
		return !session.token ? <Component /> : <Redirect to={redirect} />;
	else
		return <Component />;
};

const mapStateToProps = (state: any) => ({
	session: state.session
});

export default connect(mapStateToProps, { thunkCheckAuth })(View);