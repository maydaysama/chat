import React from 'react';
import View from './View';
import { Create } from '../account';

const CreateView = () => {
	return (
		<View Component={Create} unlogged redirect="/" />
	);
};

export default CreateView;