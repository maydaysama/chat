import React from 'react';
import View from './View';
import { Profile } from '../profile';

const ProfileView = () => {
	return (
		<View Component={Profile} logged redirect="/login" />
	);
};

export default ProfileView;