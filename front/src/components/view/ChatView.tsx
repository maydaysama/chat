import React from 'react';
import View from './View';
import { Chat } from '../chat';

const ChatView = () => {
	return (
		<View Component={Chat} />
	);
};

export default ChatView;