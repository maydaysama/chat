import React, { useEffect } from 'react';
import { makeStyles, Paper } from '@material-ui/core';
import { connect } from 'react-redux';
import { thunkFetchMessages, thunkFetchUsers } from '../../store/';
import Message from './Message';
import { Message as MessageType } from '../../types';

const useStyles = makeStyles(theme => ({
	cont: {
		background: theme.palette.grey[100],
		overflowY: "scroll",
		height: "100%",
	}
}));

interface MessageListProps {
	messages: MessageType[],
	thunkFetchMessages: any,
	thunkFetchUsers: any
};

export const MessageList: React.FC<MessageListProps> = ({ messages, thunkFetchMessages, thunkFetchUsers }) => {
	const classes = useStyles();
	let messagesEnd: HTMLDivElement | null = null;

	useEffect(() => {
		thunkFetchMessages();
		thunkFetchUsers();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		if (messagesEnd)
			messagesEnd.scrollIntoView();
	});
	return (
		<Paper id="message-list-container" className={classes.cont}>
			{messages.map((message: MessageType) => {
				return (
					<Message
						key={message._id}
						message={message}
					/>
			)})}
			<div style={{ float:"left", clear: "both" }} ref={(el) => { messagesEnd = el; }}></div>
		</Paper>
	);
};

const mapStateToProps = (state: any) => ({
	messages: state.messages,
});

export default connect(mapStateToProps, { thunkFetchMessages, thunkFetchUsers })(MessageList);