import React, { useEffect } from 'react';
import MessageList from './MessageList';
import Send from './Send';
import { makeStyles, Container } from '@material-ui/core';
import { connect } from 'react-redux';
import socket from '../../api/socket';
import { addMessage } from '../../store/messages';
import { Message } from '../../types';

const useStyles = makeStyles(theme => ({
	cont: {
		width: "100%",
		padding: "0",
		height: "77vh",
	}
}));

const Chat = ({ addMessage }: any) => {
	const classes = useStyles();

	useEffect(() => {
		const newMessage = (data: Message) => addMessage(data);
		socket.on("newMessage", newMessage);
		socket.open();
		return () => {
			socket.removeListener("newMessage", newMessage);
			socket.disconnect();
			console.log("socket: user disconnected");
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<Container className={classes.cont}>
			<MessageList />
			<Send />
		</Container>
	);
};

const mapStateToProps = (state: any) => ({
	session: state.session,
});

export default connect(mapStateToProps, { addMessage })(Chat);