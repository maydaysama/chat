import React, { useState } from 'react';
import { TextField, Button, makeStyles, Paper } from '@material-ui/core';
import { connect } from 'react-redux';
import { MessageApi } from '../../api';
import { thunkCheckAuth } from '../../store';

const useStyles = makeStyles(theme => ({
	bar: {
		padding: "10px",
		display: "flex",
		justifyContent: "space-between",
		position: "fixed",
		bottom: 0,
		left: 0,
		width: "100%"
	},
	input: {
		width: "90%",
	}
}));

export const Send = ({ session, thunkCheckAuth }: any) => {
	const classes = useStyles();
	const [value, setValue] = useState("");
	const handleChangeValue = (evt: any) => {
		setValue(evt.target.value);
	}
	const handleKeyPress = (evt: any) => {
		if (evt.which === 13)
		{
			evt.preventDefault();
			if (evt.ctrlKey === true)
				setValue(value + "\n");
			else
				submit();			
		}	
	}
	const submit = async () => {
		if (value.trim() === "")
			return ;
		if (value !== "") {
			MessageApi.post("/", { content: value.trim() }, {
				headers: {
					'Authorization': `Bearer ${session.token}`,
				}
			})
			.then(response => {
				if (process.env.NODE_ENV !== "test")
					setValue("");
			})
			.catch(e => {
				console.log(e);
				thunkCheckAuth();
			});
		}
	};
	return (
		<Paper elevation={4} className={classes.bar}>
			<TextField
				id="standard-multiline-flexible"
				multiline
				rowsMax="4"
				value={session.token ? value : "You need to connect to chat !"}
				onChange={handleChangeValue}
				onKeyPress={handleKeyPress}
				className={classes.input}
				variant="outlined"
				disabled={session.token ? false : true}
				inputProps={{ "data-testid": "field" }}
			/>
			<Button
				variant="contained"
				color="primary"
				style={{ height: "55px"}}
				onClick={submit}
				disabled={session.token ? false : true}
				data-testid="button"
			>
				Send
			</Button>
		</Paper>
	);
};

const mapStateToProps = (state: any) => ({
	session: state.session
});

export default connect(mapStateToProps, { thunkCheckAuth })(Send);