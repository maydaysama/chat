import React from 'react';
import Form from './Form';
import { UserApi } from '../../api';
import { PopupType, FormInputs, FormFields } from '../../types';

const inputs: FormInputs = {
	username: {
		label: "Username",
		type: "text"
	},
	mail: {
		label: "Mail",
		type: "text"
	},
	password: {
		label: "Password",
		type: "password"
	},
	passwordConfirm: {
		label: "Confirm Password",
		type: "password"
	}
};

const check = (form: FormFields) => {
	const { password, passwordConfirm } = form;

	if (password !== passwordConfirm)
		throw new Error(`You did'nt enter the same password !`);
};

const submit = (form: FormFields) => {
	const { username, password, mail } = form;

	return UserApi.post("/", { username, password, mail });
};

export const popups: PopupType = {
	ok: (setPopup) => ({
		message: "User successfully created ! Please login now !",
		buttons: [
			{ label: "OK", path: "/login" },
		],
	})
};

const Create = () => {
	return (
		<Form
			title="Create"
			inputs={inputs}
			check={check}
			submit={submit}
			popups={popups}
		/>
	);
};

export default Create;