import React from 'react';
import Form from './Form';
import { UserApi } from '../../api';
import { connect } from 'react-redux';
import SHA256 from 'crypto-js/sha256';
import { logUser } from '../../store/session';
import { FormInputs, FormFields } from '../../types';

const inputs: FormInputs = {
	mail: {
		label: "Mail",
		type: "text",
	},
	password: {
		label: "Password",
		type: "password"
	},
};

const submit = (form: FormFields) => {
	const { mail, password } = form;

	return UserApi.post("/login", { mail, password: SHA256(password).toString() });
};

export const Login: React.FC<any> = ({ logUser }) => {
	return (
		<Form
			title="Login"
			inputs={inputs}
			submit={submit}
			action={logUser}
		/>
	);
};

export default connect(null, { logUser })(Login);