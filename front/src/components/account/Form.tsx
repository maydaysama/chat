import React, { useState } from 'react';
import { Paper, TextField, Container, makeStyles, Button, Typography } from '@material-ui/core';
import { Popup } from '../popup';
import { PopupInfo, FormInputs, FormFields, PopupType } from '../../types';
import { AxiosResponse } from 'axios';

const useStyles = makeStyles(theme => ({
	cont: {
		maxWidth: "500px",
		width: "80vw",
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		alignItems: "center",
	},
	title: {
		margin: "30px 0px 20px 0px",
	},
	form: {
		width: "90%",
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		margin: "20px 0px 20px 0px",
	},
	field: {
		margin: "5px 0px 5px 0px",
	},
	error: {
		color: "red",
	},
	buttons: {
		display: "flex",
		justifyContent: "center",
		margin: "20px 0px 20px 0px",
	}
}));

interface FormProps {
	title: string,
	inputs: FormInputs,
	submit: (form: FormFields) => Promise<AxiosResponse<any>>,
	popups?: PopupType,
	action?: any,
	check?: Function,
};

const Form: React.FC<FormProps> = ({ title, inputs, submit, popups, action, check }) => {
	const classes = useStyles();
	const fields = Object.keys(inputs).reduce((o, key) => ({ ...o, [key]: ""}), {});
	const [formVal, setFormVal] = useState<FormFields>(fields);
	const [popup, setPopup] = useState<PopupInfo | any>(false);
	const [error, setError] = useState<string>("");

	const handleChangeForm = (evt: React.ChangeEvent<HTMLInputElement>) => {
		setFormVal({ ...formVal, [evt.target.id]: evt.target.value });
	};

	const formSubmit = async () => {
		try {
			if (check)
				check(formVal);
			const response = await submit(formVal);
			if (action)
				action(response.data);
			if (process.env.NODE_ENV !== 'test' && popups && popups.ok)
				setPopup(popups.ok(setPopup));
		}
		catch (e) {
			const err = e.response ? e.response.data : e.message;
			setError(err);
		}
	};
	return (
		<Paper elevation={4} className={classes.cont}>
			<Container className={classes.title}>
				<Typography variant="h4" component="h4" align="center" color="primary">{title}</Typography>
			</Container>
			<Container className={classes.form}>
				{Object.keys(formVal).map(key => (
					<TextField
						key={key}
						id={key}
						value={formVal[key]}
						onChange={handleChangeForm}
						label={inputs[key].label}
						variant="outlined"
						type={inputs[key].type}
						className={classes.field}
						color="primary"
					/>
				))}
			</Container>
			<div className={classes.error} data-testid="error">
				{error}
			</div>
			<Container className={classes.buttons}>
				<Button onClick={formSubmit} variant="outlined" color="primary">OK</Button>
			</Container>
			{popup ? <Popup info={popup} /> : ""}
		</Paper>
	);
};

export default Form;