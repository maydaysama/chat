import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Grid, Container, Avatar, makeStyles, Button, fade } from '@material-ui/core';
import { UserApi } from '../../api';
import { Popup } from '../popup';
import { delogUser } from '../../store';
import { PopupType, PopupInfo } from '../../types';

const useStyles = makeStyles(theme => ({
	avatarContainer: {
		width: "80vw",
		maxWidth: "600px",
		height: "200px",
		display: "flex",
		justifyContent: "center",
		margin: "10px 10px 10px 10px",
		paddingTop: "10px",
		overflowY: "auto"
	},
	avatar: {
		width: theme.spacing(10),
		height: theme.spacing(10),
		"&:hover": {
			filter: "sepia(30%)"
		}
	},
	button: {
		margin: "10px 0px 10px 0px",
	},
	buttons: {
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		alignItems: "center",
		margin: "20px 0px 10px 0px",
	},
	select: {
		background: fade(theme.palette.primary.main, 0.5),
		//borderRadius: "100px",
		transition: "0.1s",
	},
}));

const popups: PopupType = {
	ok: (setPopup) => ({
		message: "Your profil has been updated !",
		buttons: [
			{ label: "OK", callback: () => {
				if (setPopup)
					setPopup(false);
			}
			},
			{ label: "GO TO CHAT", path: "/" }
		],
	}),
	fail: (setPopup) => ({
		message: "You must login before !",
		buttons: [
			{ label: "OK", path: "/login"},
		],
	}),
};

export const AvatarSelection = ({ session, avatars, delogUser }: any) => {
	const classes: any = useStyles();
	const [avatarId, setAvatarId] = useState<string>("avatar0");
	const [popup, setPopup] = useState<PopupInfo | any>(false);

	const handleChangeAvatar = (evt: any) => {
		if (process.env.NODE_ENV === "test")
			return ;
		const prevImg: any = document.querySelector(`#${avatarId}`);
		if (prevImg)
			prevImg.parentElement.parentElement.classList.remove(classes.select);
		setAvatarId(`${evt.target.id}`);
		const newImg: any = document.querySelector(`#${evt.target.id}`);
		newImg.parentElement.parentElement.classList.add(classes.select);
	};
	const submit = () => {
		UserApi
		.post("/modifyAvatar", { avatarId: avatarId.replace('avatar', '') }, {
			headers: {
				'Authorization': `Bearer ${session.token}`,
			},
		})
		.then(response => {
			if (process.env.NODE_ENV !== "test")
				setPopup(popups.ok(setPopup))
		})
		.catch(err => {
			console.log(err);
			setPopup(popups.fail(setPopup))
		});
	};
	return (
		<div>
			<Container className={classes.avatarContainer}>
				<Grid container justify="center" spacing={2}>
					{avatars.map((avatar: any) => (
						<Grid item key={avatar.id} className={classes.gridHover}>
							<Avatar
								src={avatar.src}
								className={classes.avatar}
								imgProps={{id: `avatar${avatar.id}`}}
								onClick={handleChangeAvatar}
								data-testid={`avatar${avatar.id}`}
							/>
						</Grid>
					))}
				</Grid>
			</Container>
			<Container className={classes.buttons}>
				<Button onClick={submit} variant="outlined" color="primary" data-testid="button" className={classes.button}>CHANGE PIC</Button>
				<Button onClick={delogUser} variant="outlined" color="primary" className={classes.button}>DELOG</Button>
			</Container>
			{popup ? <Popup info={popup} /> : ""}
		</div>
	);
};

const mapStateToProps = (state: any) => ({
	avatars: state.avatars,
	session: state.session,
});

export default connect(mapStateToProps, { delogUser })(AvatarSelection);