import React from 'react';
import { Container, Button, makeStyles } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import { PopupButtonsProps, PopupButton } from '../../types';

const useStyles = makeStyles(theme => ({
	buttons: {
		display: "flex",
		justifyContent: "center",
		marginTop: "10px",
	},
}));

const PopupButtons: React.FC<PopupButtonsProps> = ({ buttons }) => {
	const classes = useStyles();
	
	return (
		<Container className={classes.buttons}>
			{buttons.map((button: PopupButton, k) => {
				const router = button.path ? { component: RouterLink, to: button.path } : {};
				return (
					<Button
						key={k}
						color="primary"
						variant="outlined"
						onClick={button.callback}
						data-testid={`button${k}`}
						{...router}
					>
						{button.label}
					</Button>
				)})}
		</Container>
	);
};

export default PopupButtons;