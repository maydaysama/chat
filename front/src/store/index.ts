import { combineReducers } from 'redux';
import { messageReducer } from './messages';
import { usersReducer } from './users';
import { avatarReducer } from './avatars';
import { sessionReducer } from './session';

export * from "./messages";
export * from "./users";
export * from './avatars';
export * from './session';

export const rootReducer = combineReducers({
    messages: messageReducer,
    users: usersReducer,
    avatars: avatarReducer,
    session: sessionReducer,
});