const initialState = () => {
	const state = [];
	for (let i in [...Array(20).keys()]) {
		state.push({id: parseInt(i), src: `${process.env.PUBLIC_URL}/resources/avatar/${i}.jpg`});
	}
	return state;
};

export const avatarReducer = (state = initialState()) => {
	return state;
};