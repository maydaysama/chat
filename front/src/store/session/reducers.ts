import { socket } from "../../api";
import { SessionState, SessionAction, LOG_USER, DELOG_USER } from './types';

const initialState: SessionState = {
	token: false,
	user: false
};

export const sessionReducer = (state = initialState, action: SessionAction) => {
    switch (action.type) {
        case LOG_USER:
            socket.io.opts.query = { token: action.payload };
            return {
                token: action.payload,
                user: JSON.parse(window.atob(action.payload.split('.')[1]))
            };
        case DELOG_USER:
            socket.io.opts.query = {};
            return initialState;
        default:
            return state;
    };
};