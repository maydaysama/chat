import { UserApi } from "../../api";
import { LOG_USER, DELOG_USER } from "./types";

export const logUser = (data: string) => {
	return {
		type: LOG_USER,
		payload: data
	}
};

export const delogUser = () => {
	return {
		type: DELOG_USER
	};
};

export const thunkCheckAuth = () => {
	return async (dispatch: any, getState: any) => {
		if (getState().session.token)
		{
			try {
				await UserApi.post("/token", {}, {
					headers: {
						'Authorization': `Bearer ${getState().session.token}`,
					},
				});
			}
			catch (e) {
				dispatch(delogUser());
			};
		}
	};
};