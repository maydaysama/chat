import { User, Token } from "../../types";

export const LOG_USER = "LOG_USER";
export const DELOG_USER = "DELOG_USER";

interface LogUserAction {
    type: typeof LOG_USER,
    payload: string
};

interface DeLogUserAction {
    type: typeof DELOG_USER
};

export type SessionAction = LogUserAction | DeLogUserAction;

export interface SessionState {
    token: Token,
    user: User | boolean
};