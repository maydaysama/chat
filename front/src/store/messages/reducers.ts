import { MessageAction, MessageState, ADD_MESSAGE, FETCH_MESSAGES } from "./types";

const initialState: MessageState = [];

export const messageReducer = (state = initialState, action: MessageAction) => {
    switch (action.type) {
        case ADD_MESSAGE:
            return [ ...state, action.payload ];
        case FETCH_MESSAGES:
            return [ ...action.payload ];
        default:
            return state;
    };
};