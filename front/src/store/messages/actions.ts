import { MessageApi } from '../../api';
import { Message } from "../../types";
import { MessageAction, ADD_MESSAGE, FETCH_MESSAGES } from "./types";


export const addMessage = (data: Message): MessageAction => {
	return {
		type: ADD_MESSAGE,
		payload: data
	};
};

export const fetchMessages = (data: Message[]): MessageAction => {
	return {
		type: FETCH_MESSAGES,
		payload: data
	};
};

export const thunkFetchMessages = () => {
	return async (dispatch: any) => {
		try {
			const response = await MessageApi.get("/");
			dispatch(fetchMessages(response.data));
		}
		catch (e) {
			console.log("Cannot reach server");
		}
	};
};