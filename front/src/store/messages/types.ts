import { Message } from "../../types";

export const ADD_MESSAGE = "ADD_MESSAGE";
export const FETCH_MESSAGES = "FETCH_MESSAGES";

interface AddMessageAction {
    type: typeof ADD_MESSAGE,
    payload: Message
};

interface FetchMessagesAction {
    type: typeof FETCH_MESSAGES,
    payload: Message[]
};
export type MessageAction = AddMessageAction | FetchMessagesAction;

export type MessageState = Message[];

