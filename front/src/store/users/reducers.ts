import { UserState, UserAction, FETCH_USERS } from "./types";

const initialState: UserState = [];

export const usersReducer = (state = initialState, action: UserAction) => {
	switch (action.type) {
		case FETCH_USERS:
			return [ ...action.payload ];
		default:
			return state;
	};
};