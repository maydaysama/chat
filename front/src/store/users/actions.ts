import { UserApi } from '../../api';
import { User } from '../../types';
import { FETCH_USERS } from './types';

export const fetchUsers = (data: User[]) => {
    return {
        type: FETCH_USERS,
        payload: data
    };
};

export const thunkFetchUsers = () => {
	return async (dispatch: any) => {
		try {
			const response = await UserApi.get("/");
			dispatch(fetchUsers(response.data));
		}
		catch (e) {
			console.log("Cannot reach server");
		}
	};
};