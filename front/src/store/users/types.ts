import { User } from "../../types";

export const FETCH_USERS = "FETCH_USERS";

interface FetchUsersAction {
    type: typeof FETCH_USERS,
    payload: User[]
};

export type UserAction = FetchUsersAction;

export type UserState = User[];