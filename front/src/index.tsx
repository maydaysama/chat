import React from 'react';
import ReactDOM from 'react-dom';
import { Routing } from './components';
import { Provider } from 'react-redux';
import { CssBaseline } from '@material-ui/core';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from './store';

if (process.env.NODE_ENV === "production")
	console.log = () => {};

console.log(process.env.NODE_ENV);
console.log(process.env.NODE_ENV);

const App = () => (
	<Provider store={createStore(rootReducer, applyMiddleware(thunk))}>
		<CssBaseline />
		<Routing />
	</Provider>
);

ReactDOM.render(<App />, document.querySelector("#root"));