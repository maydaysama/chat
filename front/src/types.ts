/**
 * POPUP ---- interfaces
 */
export interface PopupButton {
    label?: string,
    path?: string,
    callback?: (evt: any) => void,
};

export interface PopupInfo {
    message: string,
    buttons: PopupButton[]
};

export type PopupChoice = (callback?: React.Dispatch<React.SetStateAction<any>>) => PopupInfo;

export interface PopupType {
    [key: string]: PopupChoice
};

export interface PopupButtonsProps {
	buttons: PopupButton[]
};

export interface PopupProps {
    info: PopupInfo
};

/**
 * Form
 */

export interface FormInputs {
    [key: string]: {
        label: string,
        type: string
    }
};

export interface FormFields {
    [key: string]: string
};

/**
 * message
 */

export interface Message {
    _id: string,
    content: string,
    userId: number,
    date: string,
};

/**
 * user
 */

export interface User {
    id: number,
    username: string,
    avatarId: number,
    mail: string,
};

/**
 * session
 */

export type Token = string | boolean;

/**
 * avatar
 */

export interface Avatar {
    id: number,
    src: string
};