import axios from 'axios';

const url = process.env.NODE_ENV === "production" ? "52.148.247.73" : window.location.hostname;

export default axios.create({
	baseURL: `http://${url}:8000/user/`
});