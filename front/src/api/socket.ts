import io from 'socket.io-client';

const url = process.env.NODE_ENV === "production" ? "52.148.247.73" : window.location.hostname;

const socket = io(`http://${url}:8080`, {
	autoConnect: false,
});

socket.on("connect", (soc: any) => {
	console.log("socket: user connected !");
});

export default socket;